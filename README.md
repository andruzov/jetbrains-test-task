## Requirements

`Node.js` - `v12.11.1`

## How to run project

### `npm install`

Install dependencies.

### `npm start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

Specify **TOC_DATA_URL** environment variable to change URL for fetching TOC data.

## JS API
### `TocApi.selectPageById`
Accepts single argument - page id.

### `TocApi.selectAnchorById`
Accepts single argument - anchor id.

### `TocApi.filterPagesByText`
Accepts single argument - search text. Returns found pages.
