import React, {useContext} from 'react';
import Page from 'components/Page/Page';
import NavigationContext from 'contexts/NavigationContext';

function PageList({isTopLevel, pagesIds}) {
    const {pages, topLevelIds, expandedPages, selectPage, selectedPage} = useContext(NavigationContext);

    const nestedPagesIds = isTopLevel ? topLevelIds : pagesIds;

    return (
        <ul>
            {nestedPagesIds.map(id => (
                <Page
                    key={id}
                    pageInfo={pages[id]}
                    onPageSelect={() => selectPage(id)}
                    isExpanded={expandedPages.includes(id)}
                    isSelected={selectedPage === id}
                />
            ))}
        </ul>
    );
}

export default PageList;
