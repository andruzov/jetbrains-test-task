import React from 'react';
import Header from 'containers/Header/Header';
import Article from 'containers/Article/Article';
import TableOfContents from 'containers/TableOfContents/TableOfContents';
import {NavigationContextProvider} from 'contexts/NavigationContext';
import styles from './App.module.scss';

function App() {
    return (
        <NavigationContextProvider>
            <div className={styles.app}>
                <Header/>
                <div className={styles['page-content']}>
                    <TableOfContents/>
                    <Article/>
                </div>
            </div>
        </NavigationContextProvider>
    );
}

export default App;
