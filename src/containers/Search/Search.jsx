import React, {useContext, useEffect, useRef, useState} from 'react';
import SearchResults from 'components/SearchResults/SearchResults';
import NavigationContext from 'contexts/NavigationContext';
import styles from 'containers/Search/Search.module.scss';

function Search() {
    const [searchText, setSearchText] = useState('');
    const [results, setResults] = useState([]);
    const [isTyping, setIsTyping] = useState(false);

    const typingTimeout = useRef(null);

    const {filterPagesByText, selectPage} = useContext(NavigationContext);

    function onResultClick(pageId) {
        setSearchText('');
        selectPage(pageId);
    }

    useEffect(() => {
        if (typingTimeout.current) {
            clearTimeout(typingTimeout.current);
        }

        const trimmedText = searchText.trim();

        if (trimmedText === '') {
            setIsTyping(false);
            setResults([]);
        } else {
            setIsTyping(true);
            typingTimeout.current = setTimeout(() => {
                setResults(filterPagesByText(trimmedText));
                setIsTyping(false);
            }, 500);
        }

        return () => clearTimeout(typingTimeout.current);
    }, [searchText]);

    return (
        <div className={styles.search}>
            <input
                value={searchText}
                onChange={e => setSearchText(e.target.value)}
                className={styles['search-input']}
                placeholder="Type here to find page by title"
            />
            {searchText !== '' && (
                <React.Fragment>
                    <span className={styles['clear-button']} onClick={() => setSearchText('')}>✖</span>
                    <SearchResults pages={results} isTyping={isTyping} onResultClick={onResultClick}/>
                </React.Fragment>
            )}
        </div>
    );
}

export default Search;
