import React, {useContext} from 'react';
import Anchor from 'components/Anchor/Anchor';
import NavigationContext from 'contexts/NavigationContext';

function AnchorList({anchorsIds, level}) {
    const {anchors, selectedAnchor, selectAnchor} = useContext(NavigationContext);

    return (
        <ul>
            {anchorsIds.map(id => (
                <Anchor
                    key={id}
                    anchorInfo={anchors[id]}
                    onAnchorSelect={() => selectAnchor(id)}
                    isSelected={selectedAnchor === id}
                    level={level}
                />
            ))}
        </ul>
    );
}

export default AnchorList;
