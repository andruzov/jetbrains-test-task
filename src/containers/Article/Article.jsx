import React, {useContext} from 'react';
import NavigationContext from 'contexts/NavigationContext';
import styles from 'containers/Article/Article.module.scss';

function Article() {
    const {pages, anchors, selectedPage, selectedAnchor} = useContext(NavigationContext);

    return (
        <div className={styles.article}>
            <p>Selected page: {selectedPage ? pages[selectedPage].title : 'None'}</p>
            <p>Selected anchor: {selectedAnchor ? anchors[selectedAnchor].title : 'None'}</p>
        </div>
    );
}

export default Article;
