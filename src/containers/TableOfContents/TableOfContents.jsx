import React, {useContext} from 'react';
import PageList from 'containers/PageList/PageList';
import Preloader from 'components/Preloader/Preloader';
import NavigationContext from 'contexts/NavigationContext';
import styles from './TableOfContents.module.scss';

function TableOfContents() {
    const {isTocDataFetched} = useContext(NavigationContext);

    return (
        <div className={styles.toc}>
            {isTocDataFetched ? <PageList isTopLevel/> : <Preloader/>}
        </div>
    );
}

export default TableOfContents;
