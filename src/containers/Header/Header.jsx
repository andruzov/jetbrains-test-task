import React from 'react';
import Search from 'containers/Search/Search';
import styles from './Header.module.scss';

function Header() {
    return (
        <header className={styles.header}>
            <Search/>
        </header>
    );
}

export default Header;
