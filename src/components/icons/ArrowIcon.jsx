import React from 'react';

function ArrowIcon(props) {
    return (
        <svg viewBox="-5 -3 24 24" {...props}>
            <path d="M11 9l-6 5.25V3.75z"/>
        </svg>
    );
}

export default ArrowIcon;
