import React from 'react';
import TocLink from 'components/TocLink/TocLink';

function Anchor({anchorInfo, onAnchorSelect, isSelected, level}) {
    const anchorUrl = `${anchorInfo.url}${anchorInfo.anchor}`;

    return (
        <li>
            <TocLink
                isExpandable={false}
                isSelected={isSelected}
                level={level}
                onLinkClick={onAnchorSelect}
                href={anchorUrl}
                isAnchor
            >
                {anchorInfo.title}
            </TocLink>
        </li>
    );
}

export default Anchor;
