import React from 'react';
import classNames from 'classnames';
import {SlideDown} from 'react-slidedown'
import PageList from 'containers/PageList/PageList';
import AnchorList from 'containers/AnchorList/AnchorList';
import ArrowIcon from 'components/icons/ArrowIcon';
import TocLink from 'components/TocLink/TocLink';
import styles from './Page.module.scss';
import 'react-slidedown/lib/slidedown.css'

function Page({pageInfo, onPageSelect, isExpanded, isSelected}) {
    const arrowClassName = classNames(styles.arrow, {
        [styles['arrow--expanded']]: isExpanded,
    });

    const isExpandable = pageInfo.pages && !!pageInfo.pages.length;
    const hasAnchors = pageInfo.anchors && !!pageInfo.anchors.length;

    return (
        <li>
            <TocLink
                href={pageInfo.url}
                isSelected={isSelected}
                onLinkClick={onPageSelect}
                level={pageInfo.level}
                isExpandable={isExpandable}
                isAnchor={hasAnchors && isSelected}
            >
                {isExpandable && <ArrowIcon className={arrowClassName}/>}
                {pageInfo.title}
            </TocLink>
            <SlideDown>
                {hasAnchors && isSelected && <AnchorList anchorsIds={pageInfo.anchors} level={pageInfo.level}/>}
                {isExpandable && isExpanded && <PageList pagesIds={pageInfo.pages}/>}
            </SlideDown>
        </li>
    );
}

export default Page;
