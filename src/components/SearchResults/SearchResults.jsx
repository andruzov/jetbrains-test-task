import React from 'react';
import Spinner from 'components/Spinner/Spinner';
import styles from './SearchResults.module.scss';

function SearchResults({pages, isTyping, onResultClick}) {
    function renderContent() {
        if (isTyping) {
            return <div className={styles.loading}><Spinner/></div>;
        }

        if (!pages.length) {
            return <div className={styles['no-results']}>No results</div>;
        }

        return (
            <ul>
                {pages.map(page => (
                    <SearchResultItem key={page.id} pageInfo={page} onItemClick={() => onResultClick(page.id)}/>
                ))}
            </ul>
        );
    }

    return (
        <div className={styles['search-results']}>
            {renderContent()}
        </div>
    );
}

function SearchResultItem({pageInfo, onItemClick}) {
    function onClick(e) {
        e.preventDefault();
        onItemClick();
    }

    return (
        <li>
            <a href={pageInfo.url} className={styles.link} onClick={onClick}>
                {pageInfo.title}
            </a>
        </li>
    );
}

export default SearchResults;
