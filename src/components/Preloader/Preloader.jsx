import React from 'react';
import styles from './Preloader.module.scss';

function Preloader() {
    return (
        <ul>
            <Placeholder/>
            <Placeholder level={1} marginRight={16}/>
            <Placeholder level={1}/>
            <Placeholder level={1} marginRight={16}/>
            <Placeholder level={2}/>
            <Placeholder level={2} marginRight={16}/>
            <Placeholder level={2}/>
            <Placeholder level={2} marginRight={16}/>
            <Placeholder/>
            <Placeholder/>
        </ul>
    );
}

function Placeholder({level = 0, marginRight = 0}) {
    const style = {marginLeft: `${level * 16}px`, marginRight: `${marginRight}px`};

    return <li className={styles.placeholder}><div className={styles['fake-content']} style={style}/></li>;
}

export default Preloader;
