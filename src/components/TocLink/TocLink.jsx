import React from 'react';
import classNames from 'classnames';
import styles from './TocLink.module.scss';

function TocLink({href, isSelected, onLinkClick, level, isExpandable, isAnchor, children}) {
    const linkClassName = classNames(styles.link, {
        [styles['link--selected']]: isSelected,
        [styles['link--anchor']]: isAnchor,
    });

    let linkLeftPadding = 32 + 16 * level;
    if (!isExpandable) {
        linkLeftPadding += 16;
    }
    const linkStyle = {paddingLeft: `${linkLeftPadding}px`};

    function onClick(e) {
        e.preventDefault();
        onLinkClick();
    }

    return (
        <a href={href} className={linkClassName} style={linkStyle} onClick={onClick}>
            {children}
        </a>
    );
}

export default TocLink;
