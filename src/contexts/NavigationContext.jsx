import React, {Component, createContext} from 'react';
import uniqueArray from 'util/uniqueArray';

const NavigationContext = createContext({});

const TOC_DATA_URL = process.env.TOC_DATA_URL || 'http://localhost:3000/HelpTOC.json';

export class NavigationContextProvider extends Component {
    constructor(props) {
        super(props);

        this.state = {
            pages: {},
            anchors: {},
            topLevelIds: [],
            isTocDataFetched: false,
            selectedPage: null,
            selectedAnchor: null,
            expandedPages: [],
        };

        this.actions = {
            selectPage: this.selectPage,
            selectAnchor: this.selectAnchor,
            filterPagesByText: this.filterPagesByText,
        };

        window.TocApi.selectPageById = this.selectPage;
        window.TocApi.selectAnchorById = this.selectAnchor;
        window.TocApi.filterPagesByText = this.filterPagesByText;
    }

    componentDidMount() {
        fetch(TOC_DATA_URL, {})
            .then(res => res.json())
            .then(this.onTocDataReady)
            .catch(err => console.error(err));
    }

    onTocDataReady = ({entities, topLevelIds}) => {
        this.setState({
            pages: entities.pages,
            anchors: entities.anchors,
            topLevelIds,
            isTocDataFetched: true,
        }, this.selectPageFromPath);
    };

    filterPagesByText = text => {
        const pages = Object.values(this.state.pages);
        const lowerCasedText = text.toLowerCase();
        const pageContainsText = p => p.title.toLowerCase().indexOf(lowerCasedText) >= 0;

        return pages.filter(pageContainsText);
    };

    selectPageFromPath = () => {
        const path = window.location.pathname.slice(1);
        const hash = window.location.hash;

        if (!path) return;

        if (hash) {
            const anchor = this.findAnchorFromUrl(path, hash);
            if (!anchor) return;

            this.selectAnchor(anchor.id, false);
            return;
        }

        const page = Object.values(this.state.pages).find(p => p.url === path);
        if (!page) return;
        this.selectPage(page.id, false);
    };

    findAnchorFromUrl = (path, hash) => {
        return Object.values(this.state.anchors).find(a => a.url === path && a.anchor === hash);
    };

    findAllParents = pageId => {
        const targetPage = this.state.pages[pageId];

        let parentId = targetPage.parentId;
        const parents = [];

        while (parentId) {
            parents.push(parentId);
            parentId = this.state.pages[parentId].parentId;
        }

        return parents;
    };

    calculateExpandedPages = (state, pageId) => {
        const nestedPages = state.pages[pageId].pages;
        const isExpandable = nestedPages && nestedPages.length;

        if (state.expandedPages.includes(pageId)) {
            return state.expandedPages.filter(expandedPageId => {
                return expandedPageId !== pageId && !this.findAllParents(expandedPageId).includes(pageId);
            });
        }

        const expandedPages = [...state.expandedPages, ...this.findAllParents(pageId)];
        if (isExpandable) {
            expandedPages.push(pageId);
        }

        return uniqueArray(expandedPages);
    };

    selectPage = (pageId, withPushToHistory = true) => {
        const page = this.state.pages[pageId];
        // There are pages without url. Not sure how they should be handled
        if (!page) {
            console.error('Page not found!');
            return;
        }

        if (withPushToHistory) {
            this.setUrl(page.url, page.title);
        }

        this.setState(state => ({
            selectedPage: page.id,
            expandedPages: this.calculateExpandedPages(state, page.id),
            selectedAnchor: null,
        }));
    };

    selectAnchor = (anchorId, withPushToHistory = true) => {
        const anchor = this.state.anchors[anchorId];

        if (!anchor) {
            console.error('Anchor not found!');
            return;
        }

        if (withPushToHistory) {
            this.setUrl(`${anchor.url}${anchor.anchor}`, anchor.title);
        }

        const isParentPageSelected = this.state.selectedPage === anchor.parentId;
        const expandedPages = isParentPageSelected
            ? this.state.expandedPages
            : this.calculateExpandedPages(this.state, anchor.parentId);

        this.setState({
            selectedPage: anchor.parentId,
            selectedAnchor: anchorId,
            expandedPages,
        });
    };

    setUrl(url, title) {
        window.history.pushState('', title, url);
    }

    render() {
        return (
            <NavigationContext.Provider value={{...this.state, ...this.actions}}>
                {this.props.children}
            </NavigationContext.Provider>
        );
    }
}

export default NavigationContext;
